export { HomePage } from "@Template/HomePage";
export { CalendarPage } from "@Template/CalendarPage";
export { AnalyticsPage } from "@Template/AnalyticsPage";
export { ProjectPage } from "@Template/ProjectPage";
export { ErrorPage } from "@Template/ErrorPage";
